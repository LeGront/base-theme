<?php

/**
 * Settings form.
 */
function autodialog_settings_form($form, &$form_state) {
  $form['autodialog_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#description' => t('Paths which will work autodialog'),
    '#default_value' => variable_get('autodialog_paths'),
  );

  return system_settings_form($form);
}
