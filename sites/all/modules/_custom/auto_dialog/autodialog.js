(function ($) {
    Drupal.behaviors.autodialog = {
        attach: function (context, settings) {
            $('.autodialog', context).once('autodialog', function (event) {
                var $link = $(this);
                var linkId = $link.attr('id');
                if (!linkId) {
                    linkId = Drupal.autodialog.generateId();
                    $link.attr('id', linkId);
                }

                Drupal.ajax[linkId] = new Drupal.ajax(linkId, this, {
                    progress: {type: 'throbber'},
                    url: $link.attr('href'),
                    event: 'click',
                    submit: {
                        js: true,
                        autodialog_link_id: linkId,
                        autodialog_options: Drupal.autodialog.getOptions(this)
                    }
                });
            });


            /**
             * Закрытие диалога по клику в не его
             */
            $('.ui-widget-overlay').click(function(){
                var modal = $(this).next();
                if(modal.length > 0){
                    modal.find('.ui-dialog-content').dialog('close');
                }
                else {
                    modal = modal.next();
                    if(modal.length > 0){
                        modal.find('.ui-dialog-content').dialog('close');
                    }
                    else {
                        $('.ui-dialog-content').dialog('close');
                    }
                }
            });

        }
    };

    // Делаем заголовок html
    $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
            if (!this.options.title) {
                title.html("&#160;");
            } else {
                title.html(this.options.title);
            }
        }
    }));

    Drupal.ajax.prototype.commands.autodialog = function (ajax, response, status) {
        var options = {
            title: response.title,
            modal: true,
            width: 'auto',
            minWidth: 400,
            maxWidth: 940,
            draggable: false,
            resizable: false
        };

        $.extend(options, response.options, {
            close: function () {
                $('#' + response.dialog_id).remove();
            },
        });
        $('<div id="' + response.dialog_id + '">' + response.content + '</div>').dialog(options);
        Drupal.attachBehaviors('.ui-dialog[aria-describedby="'+response.dialog_id+'"] .ui-dialog-titlebar')
        Drupal.attachBehaviors('#' + response.dialog_id);

    }

    Drupal.ajax.prototype.commands.autodialogCenter = function (ajax, response, status) {
        $('#' + response.dialog_id).dialog('option', 'position', 'center');
    }

    Drupal.autodialog = Drupal.autodialog || {
        generateId: function () {
            var index = 1;
            while (1) {
                var id = 'autodialog-' + index++;
                if ($('#' + id).length == 0) {
                    return id;
                }
            }
        },

        getOptions: function (element) {
            var options = {};
            $(element.attributes).each(function () {
                var matches = this.nodeName.match(/data-dialog-(.+)/);
                if (matches) {
                    options[matches[1]] = this.nodeValue;
                }
            });
            return options;
        }
    };
})(jQuery);
