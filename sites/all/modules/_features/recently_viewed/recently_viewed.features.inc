<?php
/**
 * @file
 * recently_viewed.features.inc
 */

/**
 * Implements hook_views_api().
 */
function recently_viewed_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
