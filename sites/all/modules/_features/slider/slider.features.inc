<?php
/**
 * @file
 * slider.features.inc
 */

/**
 * Implements hook_views_api().
 */
function slider_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function slider_image_default_styles() {
  $styles = array();

  // Exported image style: slide.
  $styles['slide'] = array(
    'label' => 'slide',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 970,
          'height' => 419,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function slider_node_info() {
  $items = array(
    'slide' => array(
      'name' => t('Слайд'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Название слайда'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
