name = Sidebar
description = A single sidebar layout page
preview = preview.png
template = sidebar-layout

regions[branding] = Branding
regions[header] = Header
regions[navigation] = Navigation
regions[highlighted] = Highlighted
regions[help] = Help
regions[sub_content] = Sub Content
regions[content] = Content
regions[sidebar_first] = First Sidebar
regions[sub_footer] = Sub Footer
regions[footer] = Footer

; Stylesheets
stylesheets[all][] = css/sidebar.layout.css